# logins

This is a playground for different methods of logging in and authenticating users.

## Implementations

### Passwordless

This implementation relies on phone numbers to do passwordless authentication by having users request a verification code and then exchanging that verification code for a session (or, logging in, to a user).
Verification codes are stored in a redis datastore keyed by the user's name, as are authentication tokens.
Authentication tokens, for now, are just random strings that are stored in the session cookie alongside the user's name. Authentication is performed by looking up the token stored for the user's name and comparing it to the token stored in the session.
Logging out is, therefore, simply dropping this session from redis.
Both verification codes and authentication tokens have TTLs. verification codes are short lived while authentication tokens live for longer (about 1 week now).
This means that users must actively sign in fairly quickly, but once signed in don't need to for a while.

#### Todo
1. actual texting of verification codes via aws
2. auth token refresh
