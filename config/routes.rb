Rails.application.routes.draw do
  get 'verification_code/new'
  post 'verification_code/create'

  get 'auth/new'
  post 'auth/create'

  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
