class User < ApplicationRecord
  attribute :phone_number
  attribute :name

  validates :phone_number, presence: true, phony_plausible: true
  validates :name, presence: true, uniqueness: true
end
