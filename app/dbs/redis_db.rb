class RedisDb
  attr_reader :redis
  def initialize redis = Redis.new
    @redis = redis
  end

  delegate :set, :setex, :del, :get, to: :redis
end
