class TokenDb
  attr_reader :redis
  def initialize redis = RedisDb.new
    @redis = redis
  end

  def create user_name:
    token = SecureRandom.urlsafe_base64
    redis.setex prefix(user_name), 1.week.seconds, token
    token
  end

  def fetch user_name:
    redis.get prefix(user_name)
  end

  def delete user_name:
    redis.del prefix(user_name)
  end

  private

  def prefix key
    "auth:#{key}"
  end
end
