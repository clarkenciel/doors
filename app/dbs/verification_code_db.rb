class VerificationCodeDb
  attr_reader :redis
  def initialize redis = RedisDb.new
    @redis = redis
  end

  def create user_name:
    code = generate
    redis.setex prefix(user_name), 1.week.seconds, code
    code
  end

  def fetch user_name:
    redis.get prefix(user_name)
  end

  def delete user_name:
    redis.del prefix(user_name)
  end

  private

  def prefix key
    "verification:#{key}"
  end

  def generate
    Array.new(5) { (10 * rand).round }.join
  end
end
