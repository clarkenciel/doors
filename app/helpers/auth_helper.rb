module AuthHelper
  def clear_verification_code
    verification_db.delete user_name: user.name
  end

  def verified?
    return unless user
    verification_db.fetch(user_name: user.name) == verification_code
  end

  def user
    User.where(name: user_name).first
  end

  def user_name
    auth_params[:name]
  end

  def verification_code
    auth_params[:verification_code]
  end

  def auth_params
    @auth_params ||= params.require(:auth).
      permit(:name, :verification_code)
  end

  def new_token
    token_db.create user_name: user.name
  end

  def verification_db
    VerificationCodeDb.new
  end

  def token_db
    TokenDb.new
  end
end
