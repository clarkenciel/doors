module VerificationCodeHelper
  def valid_user?
    User.where(name: user_name, phone_number: phone_number).any?
  end

  def generate_code
    db.create user_name: user_name
  end

  def user_name
    code_params[:name]
  end

  def phone_number
    code_params[:phone_number]
  end

  def code_params
    @code_params ||= params.require(:verification_code).
      permit(:name, :phone_number)
  end

  def db
    VerificationCodeDb.new
  end
end
