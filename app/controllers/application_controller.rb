class ApplicationController < ActionController::Base
  AuthenticationError = Class.new StandardError

  rescue_from AuthenticationError do |_|
    redirect_to(verification_code_new_path)
  end

  def authenticate_user!
    user_name = session[:user]
    token = session[:token]
    found_token = TokenDb.new.fetch(user_name: user_name)
    raise AuthenticationError unless [token, found_token].all? && found_token == token
  end
end
