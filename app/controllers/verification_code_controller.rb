class VerificationCodeController < ApplicationController
  include VerificationCodeHelper

  def new
  end

  def create
    if valid_user?
      generate_code
      redirect_to auth_new_path, flash: {
        notice: "When a text message with your verification code arrives, enter it here to log in"
      }
    else
      redirect_to verification_code_new_path, flash: {
        errors: ['The phone number and user name you entered do not match']
      }
    end
  end
end
