class AuthController < ApplicationController
  include AuthHelper

  def create
    if verified?
      clear_verification_code
      session[:token] = new_token
      session[:user] = user.name
      redirect_to user_path(user.id)
    else
      redirect_to auth_new_path
    end
  end

  def new
  end
end
